// Firefox Extensions Places utilities v0.0.1
// Copyright © 2020 Boian Berberov
//
// Released under the terms of the
// European Union Public License version 1.2 only.
//
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-11-12
//
// SPDX-License-Identifier: EUPL-1.2

FFXPlaces = {
	"getPath" : async function (bookmark) {
		const path = new Array();

		let curParentId = bookmark.parentId;
		while (curParentId !== "root________")
		{
			let parent = ( await browser.bookmarks.get(curParentId) )[0];
			path.unshift(parent.title);
			curParentId = parent.parentId;
		}

		return path;
	}
	,
	"getVisits" : async function(url) {
		return (
			await browser.history.getVisits( {"url": url} )
		).filter(
			function(element) {
				return element.transition !== "reload";
			}
		);
	}
	,
	"getBookmarks" : async function(url) {
		/**
		 * @param url of type URL
		 **/
		const bookmark_data = new Object();
		bookmark_data['url'] = url;

		// BUG: https://bugzilla.mozilla.org/show_bug.cgi?id=1352835
		if (
			   url.protocol === 'http:'
			|| url.protocol === 'https:'
		)
		{
			bookmark_data['bookmarks'] = await browser.bookmarks.search( {"url" : url.href} );

			if (1 <= bookmark_data.bookmarks.length)
			{
				// Unique for bookmark
				bookmark_data['visits'] = await this.getVisits(url.href);
				console.log(bookmark_data['visits'])

				// Multiple for bookmark
				for (let bookmark of bookmark_data.bookmarks)
				{
					bookmark['path'] = await this.getPath(bookmark);
				}
			}
		}
		else
		{
			bookmark_data['bookmarks'] = new Array();
			bookmark_data['visits'] = new Array();
		}

		return bookmark_data;
	}
}
