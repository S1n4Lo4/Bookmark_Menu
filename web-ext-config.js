module.exports = {
	ignoreFiles: [
		'./README.md',
		'./_images/icon.png',
		'./web-ext-config.js',
	],

	build: {
		overwriteDest: true,
	},
};
