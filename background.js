// Bookmark Menu v0.0.2
// Copyright © 2020, 2021 Boian Berberov
//
// Released under the terms of the
// European Union Public License version 1.2 only.
//
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-11-12
//
// SPDX-License-Identifier: EUPL-1.2

// "bookmark" context for Library window supported as of FF 66
// https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/menus/ContextType

'use strict';

const top_places_folders = [
	"menu________",
	"toolbar_____",
	"unfiled_____"
]

const options_default = {
	quick_move_ids : [ "toolbar_____" ]
}

//
// Initialization
//

browser.menus.onShown.addListener(
	async function(info, tab) {
		if ( info.hasOwnProperty('bookmarkId') )
		{
			function create_top_menu(title) {
				browser.menus.create(
					{
						id      : "top-menu",
						title   : title,
						contexts: [ "bookmark" ]
					}
				);
			}

			const place = ( await browser.bookmarks.get(info.bookmarkId) )[0];

			const options_local_promise = browser.storage.local.get(options_default);
			let quick_move_ids;

			switch (place.type)
			{
				case 'bookmark':
					create_top_menu( browser.i18n.getMessage("menuBookmark") );

					const bookmark_path = await FFXPlaces.getPath(place);
					const bookmark_path_end = bookmark_path.length - 1;
					browser.menus.create(
						{
							id       : "parent-" + bookmark_path_end,
							title    : browser.i18n.getMessage("menuPath", bookmark_path[bookmark_path_end]),
							contexts : [ "bookmark" ],
							parentId : "top-menu"
						}
					);
					for ( let parent_num = bookmark_path_end - 1; 0 <= parent_num; parent_num-- )
					{
						browser.menus.create(
							{
								id       : "parent-" + parent_num,
								title    : bookmark_path[ parent_num ],
								contexts : [ "bookmark" ],
								parentId : "parent-" + ( parent_num + 1 )
							}
						);
					}

					browser.menus.create(
						{
							id       : "date-added",
							title    : browser.i18n.getMessage(
								"menuDateAdded",
								( new Date(place.dateAdded) ).toLocaleDateString()
							),
							contexts : [ "bookmark" ],
							parentId : "top-menu"
						}
					);

					// Resolve quick_move_ids late for bookmarks
					quick_move_ids = ( await options_local_promise ).quick_move_ids
					break;
				case 'folder':
					create_top_menu( browser.i18n.getMessage("menuFolder") );

					// Resolve quick_move_ids early for folders
					quick_move_ids = ( await options_local_promise ).quick_move_ids

					if ( quick_move_ids.includes(place.id) )
					{
						browser.menus.create(
							{
								id       : "remove-quick-move",
								title    : browser.i18n.getMessage("menuRemoveFromQuickMove"),
								onclick  : function() {
									browser.storage.local.set(
										{ quick_move_ids : quick_move_ids.filter( element => element !== place.id ) }
									);
								},
								contexts : [ "bookmark" ],
								parentId : "top-menu"
							}
						);
					}
					else
					{
						browser.menus.create(
							{
								id       : "add-quick-move",
								title    : browser.i18n.getMessage("menuAddToQuickMove"),
								onclick  : function() {
									browser.storage.local.set(
										{ quick_move_ids : quick_move_ids.concat(place.id) }
									);
								},
								contexts : [ "bookmark" ],
								parentId : "top-menu"
							}
						);
					}
					break;
				case 'separator':
					break;
				default:
					console.log("place.type: " + place.type);
			}

			// Quick Move menu

			// Skip Quick Move menu creation for top level folders
			if ( ! top_places_folders.includes(place.id) )
			{
				browser.menus.create(
					{
						id       : "quick-move-separator",
						type     : "separator",
						contexts : [ "bookmark" ],
						parentId : "top-menu"
					}
				);
				browser.menus.create(
					{
						id       : "quick-move",
						title    : browser.i18n.getMessage("menuQuickMove"),
						contexts : [ "bookmark" ],
						parentId : "top-menu"
					}
				);

				if ( 0 < quick_move_ids.length )
				{
					const quick_move_places = await browser.bookmarks.get(quick_move_ids);

					for ( let quick_move_place of quick_move_places )
					{
						browser.menus.create(
							{
								id       : "quick-move-" + quick_move_place.id,
								title    : quick_move_place.title,
								contexts : [ "bookmark" ],
								onclick  : async function() {
									await browser.bookmarks.move(
										place.id, { parentId : quick_move_place.id }
									);
								},
								parentId : "quick-move"
							}
						);
					}
					browser.menus.create(
						{
							id       : "reset-quick-move-separator",
							type     : "separator",
							contexts : [ "bookmark" ],
							parentId : "quick-move"
						}
					);
				}
				browser.menus.create(
					{
						id       : "reset-quick-move",
						title    : browser.i18n.getMessage("menuResetQuickMove"),
						onclick  : async function() {
							await browser.storage.local.clear()
						},
						contexts : [ "bookmark" ],
						parentId : "quick-move"
					}
				);
			}
			browser.menus.refresh();
		}
	}
);

browser.menus.onHidden.addListener(
	function() {
		browser.menus.remove("top-menu");
		browser.menus.refresh();
	}
);
