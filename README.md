# Bookmark Menu

Provides an pop-up menu, displaying the bookmark's path, and allowing quick moves to a choice of folders.

## License

Licensed under the [EUPL-1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12) only.
